# GraphQL Test

## Backend

Web-server will be running on port **3003**.

GraphQL endpoint is available with URI `/graphql`

### Installation

```bash
cd backend
npm install

```

### Run

```bash
cd backend
npm start

```

## Frontend

### Installation & Build

```bash
cd frontend
npm install
npm run build

```
