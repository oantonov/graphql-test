const { buildSchema } = require('graphql');

module.exports = buildSchema(`
  type Query {
    commits: [Commit]
    commit(id: String): CommitDetails
  }

  type Commit {
    id: String!
    title: String
    created_at: String
    author: Author
  }

  type CommitDetails {
    id: String!
    title: String
    created_at: String
    author: Author
    affected_files: [File]
  }

  type Author {
    email: String!
    name: String!
  }

  type File {
    file_name: String!
  }
`);
