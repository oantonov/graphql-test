const axios = require('axios');
const config = require('./../config');

const resolver = {
  commits: async () => {
    const response = await axios.get(config.API_URL + 'repository/commits');
    return response.data.map(commit => commitReducer(commit));
  },
  commit: async (params) => {
    const response = await axios.get(config.API_URL + 'repository/commits/' + params.id);
    const responseDiff = await axios.get(config.API_URL + 'repository/commits/' + params.id + '/diff');
    return commitDetailsReducer(response.data, responseDiff.data);
  },
};

function commitReducer(commit) {
  return {
    id: commit.id,
    title: commit.title,
    created_at: commit.created_at,
    author: {
      email: commit.author_email,
      name: commit.author_name,
    },
  };
}

function commitDetailsReducer(commit, diff) {
  return {
    id: commit.id,
    title: commit.title,
    created_at: commit.created_at,
    author: {
      email: commit.author_email,
      name: commit.author_name,
    },
    affected_files: diff.map((diffItem) => ({
      file_name: diffItem.new_path
    })),
  };
}

module.exports = resolver;
