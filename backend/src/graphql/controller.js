const graphqlHTTP = require('express-graphql');
const schema = require('./schema');
const resolver = require('./resolver');

module.exports = graphqlHTTP({
  schema: schema,
  rootValue: resolver,
  graphiql: true,
});
