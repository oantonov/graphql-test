module.exports = {
    API_URL: 'https://gitlab.com/api/v4/projects/10588875/',
    ENTRY_FILE_PATH: __dirname + '/../../frontend/build/index.html',
    STATIC_FILES_PATH: __dirname + '/../../frontend/build',
};
