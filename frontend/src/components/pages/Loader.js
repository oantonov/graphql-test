import React from 'react';
import PropTypes from 'prop-types';

import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

import withRoot from './../../withRoot';

const Loader = ({ classes }) => {
  return (
    <div className={classes.root}>
      <Typography variant="h6" gutterBottom>
        Loading...
      </Typography>
    </div>
  );
}

Loader.propTypes = {
  classes: PropTypes.object.isRequired,
};

const styles = theme => ({
  root: {
    textAlign: 'center',
    marginTop: '20px',
  },
});

export default withRoot(withStyles(styles)(Loader));
