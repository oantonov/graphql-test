import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom'

import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

import withRoot from './../../withRoot';

const StartButton = withRouter(({ history }) => (
  <Button variant="contained" color="secondary"
    onClick={() => { history.push('/commits') }}
  >
    Start
  </Button>
))

const HomePage = (props) => {
  const { classes } = props;
  return (
    <div className={classes.root}>
      <Typography variant="h4" gutterBottom>
        GraphQL Test Application
      </Typography>
      <Typography variant="subtitle1" gutterBottom>
        Click the button to start
      </Typography>
      <StartButton />
    </div>
  );
}

HomePage.propTypes = {
  classes: PropTypes.object.isRequired,
};

const styles = theme => ({
  root: {
    textAlign: 'center',
    paddingTop: theme.spacing.unit * 20,
  },
});

export default withRoot(withStyles(styles)(HomePage));
