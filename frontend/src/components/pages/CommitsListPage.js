import React from 'react';
import { Query } from "react-apollo";
import { withRouter, Link } from "react-router-dom";

import Button from '@material-ui/core/Button';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { withStyles } from '@material-ui/core/styles';
import withRoot from './../../withRoot';

import Loader from './Loader';

import { GET_COMMITS } from './../GraphQL';

const BackButton = withRouter(({ history }) => (
  <Button variant="contained" color="secondary"
    onClick={() => { history.push('/') }}
  >
    Go back
  </Button>
));

const CommitsListPage = ({ classes }) => (
  <div className={classes.root}>
    <BackButton />
    <Query query={GET_COMMITS}>
      {({ loading, error, data }) => {
        if (loading) return <Loader/>;
        if (error) return `Error! ${error.message}`;

        return (
          <Paper className={classes.root}>
            <Table className={classes.table}>
              <TableHead>
                <TableRow>
                  <TableCell>Commit title</TableCell>
                  <TableCell align="center">Contributor</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {data.commits.map(commit => (
                  <TableRow key={commit.id}>
                    <TableCell component="th" scope="row">
                      <Link to={'/commits/' + commit.id}>{commit.title}</Link> 
                    </TableCell>
                    <TableCell align="center">{commit.author.name}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </Paper>
        );
      }}
    </Query>
  </div>
);

const styles = theme => ({
  root: {
    textAlign: 'left',
    maxWidth: '800px',
    margin: '20px auto 0',
  },
});

export default withRoot(withStyles(styles)(CommitsListPage));
