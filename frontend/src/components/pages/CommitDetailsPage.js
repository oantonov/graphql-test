import React from 'react';
import { Query } from "react-apollo";
import { withRouter } from 'react-router-dom';

import Paper from '@material-ui/core/Paper';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import FolderIcon from '@material-ui/icons/Folder';
import withRoot from './../../withRoot';

import Loader from './Loader';

import { GET_COMMIT_DETAILS } from './../GraphQL';

const BackButton = withRouter(({ history }) => (
  <Button variant="contained" color="secondary"
    onClick={() => { history.push('/commits') }}
  >
    Go back
  </Button>
));

const CommitDetailsPage = ({ match, classes }) => (
  <div className={classes.root}>
    <BackButton/>
    <Query query={GET_COMMIT_DETAILS} variables={{ id: match.params.id }}>
      {({ loading, error, data }) => {
        if (loading) return <Loader/>;
        if (error) return `Error! ${error.message}`;
  
        const { commit } = data;

        return (
          <>
            <Paper className={classes.infoBlock}>
              <Typography variant="h4" gutterBottom>
                {commit.title}
              </Typography>
              <Typography variant="subtitle1" gutterBottom>
                <strong>SHA1:</strong> {commit.id}
              </Typography>
              <Typography variant="subtitle1" gutterBottom>
                <strong>Date:</strong> {commit.created_at}
              </Typography>
              <Typography variant="subtitle1" gutterBottom>
                <strong>Author:</strong> {commit.author.name} ({commit.author.email})
              </Typography>
            </Paper>

            <Typography variant="h5" gutterBottom className={classes.list}>
                Affected files
              </Typography>
            <Paper>
              <List dense={true}>
                {commit.affected_files.map((file) => (
                  <ListItem key={file.file_name}>
                    <ListItemIcon>
                      <FolderIcon />
                    </ListItemIcon>
                    <ListItemText primary={file.file_name} />
                  </ListItem>
                ))}
              </List>
            </Paper>
          </>
        );
      }}
    </Query>
  </div>
);

const styles = theme => ({
  root: {
    textAlign: 'left',
    maxWidth: '800px',
    margin: '20px auto 0',
  },
  infoBlock: {
    padding: '10px',
    marginTop: '20px',
  },
  list: {
    marginTop: '20px',
  }
});
  
export default withRoot(withStyles(styles)(CommitDetailsPage));
