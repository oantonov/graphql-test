import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { HttpLink } from 'apollo-link-http';
import gql from "graphql-tag";

export const createApolloClient = () => {
    return new ApolloClient({
      cache: new InMemoryCache(),
      link: new HttpLink({
        uri: 'http://localhost:3003/graphql',
      }),
    });
};

export const GET_COMMITS = gql`
  query getCommits {
    commits {
        id
        title
        created_at
        author {
            email
            name
      }
    }
  }
`;

export const GET_COMMIT_DETAILS = gql`
  query getCommit($id: String!) {
    commit(id: $id) {
        id
        title
        created_at
        author {
            email
            name
        }
        affected_files {
            file_name
        }
    }
  }
`;
