import React from 'react';
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { ApolloProvider } from "react-apollo";

import { createApolloClient } from './GraphQL';
import HomePage from './pages/HomePage';
import CommitsListPage from './pages/CommitsListPage';
import CommitDetailsPage from './pages/CommitDetailsPage';

const apolloClient = createApolloClient();

class App extends React.Component {
    render() {
        return (
            <ApolloProvider client={apolloClient}>
                <BrowserRouter>
                    <Switch>
                        <Route path="/commits/:id" component={CommitDetailsPage}/>
                        <Route path="/commits" component={CommitsListPage}/>
                        <Route path="/" component={HomePage}/>
                    </Switch>
                </BrowserRouter>
            </ApolloProvider>
        );
    }
}

export default App;
